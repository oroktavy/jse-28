package ru.aushakov.tm.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.model.Project;
import ru.aushakov.tm.model.Task;
import ru.aushakov.tm.model.User;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@XmlRootElement(name = "data")
@XmlAccessorType(XmlAccessType.FIELD)
public class Domain implements Serializable {

    @Nullable
    @XmlElementWrapper(name = "projects")
    @XmlElement(name = "project")
    @JsonProperty("project")
    private List<Project> projects;

    @Nullable
    @XmlElementWrapper(name = "tasks")
    @XmlElement(name = "task")
    @JsonProperty("task")
    private List<Task> tasks;

    @Nullable
    @XmlElementWrapper(name = "users")
    @XmlElement(name = "user")
    @JsonProperty("user")
    private List<User> users;

}
