package ru.aushakov.tm.repository;

import ru.aushakov.tm.api.repository.IProjectRepository;
import ru.aushakov.tm.model.Project;

public final class ProjectRepository extends AbstractBusinessRepository<Project> implements IProjectRepository {
}
