package ru.aushakov.tm.api.service;

import ru.aushakov.tm.dto.Domain;

public interface IDataService {

    String getFileBinary();

    String getFileBase64();

    String getFileJson();

    String getFileXml();

    String getFileXmlForJaxB();

    Domain getDomain();

    void setDomain(Domain domain);

}
