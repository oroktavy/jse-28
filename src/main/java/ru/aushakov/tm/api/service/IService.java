package ru.aushakov.tm.api.service;

import ru.aushakov.tm.api.repository.IRepository;
import ru.aushakov.tm.model.AbstractEntity;

public interface IService<E extends AbstractEntity> extends IRepository<E> {

}
