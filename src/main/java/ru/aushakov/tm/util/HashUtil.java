package ru.aushakov.tm.util;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.exception.general.CanNotEncryptPasswordException;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

public interface HashUtil {

    @NotNull
    static String salt(
            @Nullable final String value,
            @NotNull final String secret,
            @NotNull final Integer iterationNum
    ) {
        if (StringUtils.isEmpty(value)) throw new CanNotEncryptPasswordException();
        if (iterationNum < 1) throw new CanNotEncryptPasswordException(iterationNum);
        @NotNull String result = value;
        for (int i = 0; i < iterationNum; i++) {
            result = md5(secret + result + secret);
        }
        return result;
    }

    @NotNull
    static String md5(@NotNull final String value) {
        try {
            @NotNull MessageDigest md = MessageDigest.getInstance("MD5");
            final byte[] byteArray = md.digest(value.getBytes(StandardCharsets.UTF_8));
            @NotNull final StringBuffer buffer = new StringBuffer();
            for (int i = 0; i < byteArray.length; i++) {
                buffer.append(Integer.toHexString((byteArray[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return buffer.toString();
        } catch (@NotNull Exception e) {
            throw new CanNotEncryptPasswordException(e);
        }
    }

}
