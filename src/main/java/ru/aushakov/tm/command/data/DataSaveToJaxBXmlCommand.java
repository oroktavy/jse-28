package ru.aushakov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.api.service.IDataService;
import ru.aushakov.tm.command.AbstractDataCommand;
import ru.aushakov.tm.constant.TerminalConst;
import ru.aushakov.tm.dto.Domain;
import ru.aushakov.tm.enumerated.Role;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.FileOutputStream;

public final class DataSaveToJaxBXmlCommand extends AbstractDataCommand {

    @Override
    @NotNull
    public String getName() {
        return TerminalConst.DATA_SAVE_TO_JAXB_XML;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Save application data to xml format with JaxB";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE TO XML WITH JAXB]");
        @NotNull final IDataService dataService = serviceLocator.getDataService();
        @NotNull final Domain domain = dataService.getDomain();
        @NotNull final String fileXml = dataService.getFileXmlForJaxB();

        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(fileXml);
        jaxbMarshaller.marshal(domain, fileOutputStream);
        fileOutputStream.close();
    }

}
