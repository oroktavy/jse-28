package ru.aushakov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.api.service.IDataService;
import ru.aushakov.tm.command.AbstractDataCommand;
import ru.aushakov.tm.constant.TerminalConst;
import ru.aushakov.tm.dto.Domain;
import ru.aushakov.tm.enumerated.Role;
import sun.misc.BASE64Decoder;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class DataLoadFromBase64Command extends AbstractDataCommand {

    @Override
    @NotNull
    public String getName() {
        return TerminalConst.DATA_LOAD_FROM_BASE64;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Load application data from base64 format";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA LOAD FROM BASE64]");
        @NotNull final IDataService dataService = serviceLocator.getDataService();
        @NotNull final String fileBase64 = dataService.getFileBase64();
        @NotNull final String base64 = new String(Files.readAllBytes(Paths.get(fileBase64)));
        final byte[] bytes = new BASE64Decoder().decodeBuffer(base64);
        @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        dataService.setDomain(domain);
        objectInputStream.close();
        byteArrayInputStream.close();
        serviceLocator.getAuthService().logout();
    }

}
