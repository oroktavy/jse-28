package ru.aushakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.api.service.*;
import ru.aushakov.tm.dto.Domain;
import ru.aushakov.tm.enumerated.ConfigProperty;
import ru.aushakov.tm.exception.empty.EmptyDomainException;

import java.util.Optional;

public final class DataService implements IDataService {

    @NotNull
    private final String fileBinary;

    @NotNull
    private final String fileBase64;

    @NotNull
    private final String fileJson;

    @NotNull
    private final String fileXml;

    @NotNull
    private final String fileXmlForJaxB;

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final ITaskService taskService;

    @NotNull
    private final IUserService userService;

    public DataService(
            @NotNull final IPropertyService propertyService,
            @NotNull final IProjectService projectService,
            @NotNull final ITaskService taskService,
            @NotNull final IUserService userService
    ) {
        this.fileBinary = propertyService.getProperty(ConfigProperty.DATAFILE_BINARY);
        this.fileBase64 = propertyService.getProperty(ConfigProperty.DATAFILE_BASE64);
        this.fileJson = propertyService.getProperty(ConfigProperty.DATAFILE_JSON);
        this.fileXml = propertyService.getProperty(ConfigProperty.DATAFILE_XML);
        this.fileXmlForJaxB = propertyService.getProperty(ConfigProperty.DATAFILE_JAXB_XML);
        this.projectService = projectService;
        this.taskService = taskService;
        this.userService = userService;
    }

    @Override
    @NotNull
    public String getFileBinary() {
        return fileBinary;
    }

    @Override
    @NotNull
    public String getFileBase64() {
        return fileBase64;
    }

    @Override
    @NotNull
    public String getFileJson() {
        return fileJson;
    }

    @Override
    @NotNull
    public String getFileXml() {
        return fileXml;
    }

    @Override
    @NotNull
    public String getFileXmlForJaxB() {
        return fileXmlForJaxB;
    }

    @Override
    @NotNull
    public Domain getDomain() {
        @NotNull final Domain domain = new Domain();
        domain.setProjects(projectService.findAll());
        domain.setTasks(taskService.findAll());
        domain.setUsers(userService.findAll());
        return domain;
    }

    @Override
    public void setDomain(@Nullable final Domain domain) {
        Optional.ofNullable(domain).orElseThrow(EmptyDomainException::new);
        projectService.clear();
        projectService.addAll(domain.getProjects());
        taskService.clear();
        taskService.addAll(domain.getTasks());
        userService.clear();
        userService.addAll(domain.getUsers());
    }

}
