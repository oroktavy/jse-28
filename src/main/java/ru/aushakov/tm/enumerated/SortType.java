package ru.aushakov.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.api.entity.*;

import java.util.Comparator;

public enum SortType {

    NAME("Sort by name", Comparator.comparing(IHasName::getName)),
    STATUS("Sort by status", Comparator.comparing(IHasStatus::getStatus)),
    CREATED("Sort by created date", Comparator.comparing(IHasCreated::getCreated)),
    START_DATE("Sort by start date", Comparator.comparing(IHasStartDate::getStartDate)),
    END_DATE("Sort by end date", Comparator.comparing(IHasEndDate::getEndDate));

    @NotNull
    private final String displayName;

    @NotNull
    private final Comparator comparator;

    @NotNull
    private static final SortType[] staticValues = values();

    SortType(@NotNull String displayName, @NotNull Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

    @NotNull
    public Comparator getComparator() {
        return comparator;
    }

    @Nullable
    public static SortType toSortType(@Nullable final String sortTypeId) {
        for (@NotNull final SortType sortType : staticValues) {
            if (sortType.name().equalsIgnoreCase(sortTypeId)) return sortType;
        }
        return null;
    }

}
