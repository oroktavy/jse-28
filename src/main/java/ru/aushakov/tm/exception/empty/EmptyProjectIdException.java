package ru.aushakov.tm.exception.empty;

public class EmptyProjectIdException extends RuntimeException {

    public EmptyProjectIdException() {
        super("Provided project id is empty!");
    }

}
