package ru.aushakov.tm.exception.empty;

public class EmptyDomainException extends RuntimeException {

    public EmptyDomainException() {
        super("No domain provided!");
    }

}
